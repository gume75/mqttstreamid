#!/usr/bin/env python

import paho.mqtt.client as mqtt
import json
import multiprocessing
import time
import requests

workers = {}


"""
ICY metadata read. The audio stream is a chunk of audio + a chunk of metadata
Stream sreading should add the "Icy-MetaData" header to show the capability for reading
The audio chunk length is returned in the "icy-metaint" header. The metadata chunk is
then 1 byte lenth * 16 and then this much data. 0 means no data. Metadata is just at the
begining of the songs.
"""

def openIcyStream(url):
    r = requests.get(url, headers={'Icy-MetaData': '1'}, stream=True)
    print(r.headers)
    if 'icy-metaint' not in r.headers:
        return (None, None)
    streamInfo = {}
    streamInfo['mediaSize'] = int(r.headers['icy-metaint'])
    if 'icy-name' in r.headers:
        streamInfo['icyName'] = r.headers['icy-name']
    if 'X-RadioName' in r.headers:
        streamInfo['radioName'] = r.headers['X-RadioName']
    return (r, streamInfo)

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("streamid/urlquery/+")

def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))

def on_urlquery(client, userdata, msg):
    id = msg.topic[msg.topic.rfind('/')+1:]
    url = msg.payload

    if id in workers:
        print('Change ' + id)
        workers[id].terminate()
        del workers[id]
    if url != b'':
        workers[id] = multiprocessing.Process(target=streamIdProc, args=(id, url,))
        workers[id].start()
    else:
        streamIdProcOff(id)

def streamIdProc(id, url):
    (r, streamInfo) = openIcyStream(url)
    if r is None:
        return
    ret = client.publish('streamid/streaminfo/' + id, json.dumps(streamInfo))
    client.loop_write()

    while True:
        for dummy in r.iter_content(streamInfo['mediaSize']):
            break
        for data in r.iter_content(1):
            metaSize = ord(data)*16
            if metaSize > 0:
                for metaContent in r.iter_content(metaSize):
                    song = metaContent.decode().split("'")[1]
                    client.publish('streamid/songname/' + id, '{"Song": "' + song + '"}')
                    client.loop_write()
                    break
            break


def streamIdProcOff(id):
    ret = client.publish('streamid/streaminfo/' + id, '{"icyName": ""}')
    client.loop_write()
    ret = client.publish('streamid/songname/' + id, '{"Song": ""}')
    client.loop_write()

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.message_callback_add("streamid/urlquery/+", on_urlquery)

client.connect("192.168.2.3", 1883, 60)
